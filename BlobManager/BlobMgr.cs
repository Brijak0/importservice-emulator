﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Net;

namespace BlobManager
{
    public class BlobMgr
    {

        //*********************************************************************
        // CONSTANTS
        //*********************************************************************
        #region Constants

        const string CSUPLOAD = "uploads";
        const string DEVENV = "UseDevelopmentStorage=true";
        const string CONTAINERNAME = "lastdt";
        #endregion

        //*********************************************************************
        // FIELDS
        //*********************************************************************
        #region Fields

        string _connectionStr = string.Empty;
        string _connectionStrCSBlobStore = string.Empty;
        string _lastDtBlob;
        
        string _blobName = "hh3178lastdt";
        DateTime _firstDt = new DateTime();
        

        #endregion

        //*********************************************************************
        // LIFECYCLE
        //*********************************************************************
        #region Lifecycle

        public BlobMgr(string connStr, string connStrCSBlobStore)
        {
            _connectionStr = connStr;
            _connectionStrCSBlobStore = connStrCSBlobStore;
                       
        }

        #endregion

        //*********************************************************************
        // PROPERTIES
        //*********************************************************************
        #region Properties

        internal AccessCondition BlobAccess { get; set; }
        internal CloudBlockBlob BlockBlob { get; set; }
        internal CloudBlobContainer Container { get; set; }

        #endregion

        //*********************************************************************
        // PUBLIC
        //*********************************************************************
        #region Public
        public DateTime GetLastDateTime()
        {
            DateTime lastDt = DateTime.MinValue;
            // Retrieve storage account from connection string.
            try
            {
                InitMgrForTSUpdate();
            // Acquire lease for 15 seconds
            if (!BlockBlob.Exists())
            {
                createLastDtBlob();
            }

            string lease = BlockBlob.AcquireLease(TimeSpan.FromSeconds(15), null);
            MemoryStream memoryStream = new MemoryStream();
            BlobAccess = AccessCondition.GenerateLeaseCondition(lease);
                    
           
               BlockBlob.DownloadToStream(memoryStream, BlobAccess);
               lastDt = GetDtFromStream(memoryStream);
            }
            catch (StorageException ex)
            {
                if (ex.RequestInformation.HttpStatusCode == (int)HttpStatusCode.PreconditionFailed)
                {
                    Console.WriteLine("Precondition failure as expected. Blob's lease does not match");
                }
                else
                {
                    throw;
                }
            }

            // Releasee lease proatively. Lease expires anyways after 15 seconds
            BlockBlob.ReleaseLease(BlobAccess);
            Console.WriteLine();
            
            return lastDt;
        }

        public bool SetLastDateTime(DateTime dt)
        {
            bool success = false;
            // Retrieve storage account from connection string.
            try
            {
                InitMgrForTSUpdate();
                BlockBlob.Delete();
                BlockBlob.UploadText(dt.Ticks.ToString());
                success = true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(String.Format("Exception ex ex {0}", ex.Message));
            }
            return success;
        }

        public MemoryStream GetCSBlobForAssignment(string blobFolder, string subFolder, string blobName)
        {
            MemoryStream result = new MemoryStream();
            CloudStorageAccount storageAccount;

            if (_connectionStrCSBlobStore == DEVENV)
                storageAccount = CloudStorageAccount.DevelopmentStorageAccount;
            else
            {
                storageAccount = CloudStorageAccount.Parse(_connectionStrCSBlobStore);
            }
            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            Container = blobClient.GetContainerReference(CSUPLOAD);
            CloudBlobDirectory dir = Container.GetDirectoryReference(blobFolder);
            CloudBlobDirectory subDir = dir.GetDirectoryReference(subFolder);
            var list = subDir.ListBlobs();
            BlockBlob = subDir.GetBlockBlobReference(blobName);
            try
            {
                BlockBlob.DownloadToStream(result);
            }
            catch (StorageException ex)
            {
                if (ex.RequestInformation.HttpStatusCode == (int)HttpStatusCode.PreconditionFailed)
                {
                    Console.WriteLine("Precondition failure as expected. Blob's lease does not match");
                }
                else
                {
                    result = new MemoryStream();
                }
            }


            return result;
        }

        #endregion

        //*********************************************************************
        // PRIVATE
        //*********************************************************************
        #region Private

        private void InitMgrForTSUpdate()
        {
#if DEBUG
            CloudStorageAccount storageAccount = CloudStorageAccount.DevelopmentStorageAccount;
#else
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_connectionStr);
#endif
            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            Container = blobClient.GetContainerReference(CONTAINERNAME);
            BlockBlob = Container.GetBlockBlobReference(_blobName);

        }
        private DateTime GetDtFromStream(MemoryStream memoryStream)
        {
            DateTime lastDt = DateTime.MinValue;
            if (memoryStream == null)
            {
                lastDt = new DateTime(2017, 5, 4, 0, 0, 0);
                createLastDtBlob();
            }
            byte[] bytes = new byte[memoryStream.Length];
            memoryStream.Position = 0;
            memoryStream.Read(bytes, 0, (int)memoryStream.Length);
            string data = Encoding.ASCII.GetString(bytes);
            long ticks = StringToLong(data);
            lastDt = new DateTime(ticks);
            return lastDt;
        }

        private void createLastDtBlob()
        {
            long dt = new DateTime(2017, 5, 4, 0, 0, 0).Ticks;
            BlockBlob.UploadText(dt.ToString());
        }

        private long StringToLong(string str)
        {
            long result;
            if (long.TryParse(str, out result))
                return result;
            return new DateTime(2017, 5, 4, 0, 0, 0).Ticks;
        }

        #endregion
    }
}
