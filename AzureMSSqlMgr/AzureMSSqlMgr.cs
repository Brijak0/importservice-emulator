﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using AzureMSSql.Data;

namespace AzureMSSql
{
    public class AzureMSSqlMgr
    {

        //*********************************************************************
        // CONSTANTS
        //*********************************************************************
        #region Constants

        private const String USERQUERY = "select md.attempt_id as attempt_id, md.id as session_id, md.sco_id as step, md.subsco_id as sub_step, md.[group] as groupMd, md.tag as tagMd, md.meta as metaMd, md.created as createdMd, md.modified as modifiedMd, md.size as sizeMd, " +
                "p.complete as completeP, p.completed as completedP, p.created as step_start_date, p.score as scoreP, p.elapsed as step_duration, " +
                "a.[user_id] as useridA, a.manifest_id as manifest_idA, a.org_id as org_idA, a.institution_id as institutionA " +
                "from cs_upload_meta_data as md join cs_attempt as a on md.attempt_id = a.id join cs_progress as p on md.attempt_id = p.attempt_id and md.sco_id = p.step " +
                "where md.modified > @dtFrom and md.modified < @dtTo and(md.[group] like '%Exam%' or md.sco_id like '%exam%')";

        #endregion

        //*********************************************************************
        // FIELDS
        //*********************************************************************
        #region Fields

        private String _sqlConnectionStr;

        #endregion

        //*********************************************************************
        // LIFECYCLE
        //*********************************************************************
        #region Lifecycle

        public AzureMSSqlMgr(string connectionStr)
        {
            _sqlConnectionStr = connectionStr;
        }

        #endregion

        //*********************************************************************
        // PUBLIC
        //*********************************************************************
        #region Public

        public List<ExtractedUsers> GetExtractedUsers(DateTime dtFrom, DateTime dtTo)
        {

            List<ExtractedUsers> extractedUsers = new List<ExtractedUsers>();

            using (SqlConnection conn = new SqlConnection(_sqlConnectionStr))
            {
                SqlCommand command = new SqlCommand(USERQUERY, conn);
                try
                {
                    command.Parameters.AddWithValue("dtFrom", dtFrom);
                    command.Parameters.AddWithValue("dtTo", dtTo);
                    command.Connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {

                        while (reader.Read())
                        {
                            ExtractedUsers extracted = new ExtractedUsers();
                            extracted.attempt_id = (Guid)reader["attempt_id"];
                            extracted.session_id = (Guid)reader["session_id"];
                            extracted.step = GetStringFromObj(reader["step"]);
                            extracted.sub_step = GetStringFromObj(reader["sub_step"]);
                            extracted.group = GetStringFromObj(reader["groupMd"]);
                            extracted.tag = GetStringFromObj(reader["tagMd"]);
                            extracted.meta = GetStringFromObj(reader["metaMd"]);
                            extracted.created = GetDateTimeFromObj(reader["createdMd"]);
                            extracted.modified = GetDateTimeFromObj(reader["modifiedMd"]);
                            extracted.size = (int)reader["sizeMd"];
                            extracted.complete = (int)reader["completeP"];
                            extracted.completed = GetDateTimeFromObj(reader["completedP"]);
                            extracted.step_start_date = GetDateTimeFromObj(reader["step_start_date"]);
                            extracted.score = (double)reader["scoreP"];
                            extracted.step_duration = GetDateTimeFromObj(reader["step_duration"]);
                            extracted.user_id = (Guid)(reader["useridA"]);
                            extracted.manifest_id = GetStringFromObj(reader["manifest_idA"]);
                            extracted.org_id = (Guid)reader["org_idA"];
                            extracted.institution_id = (Guid)reader["institutionA"];

                            extractedUsers.Add(extracted);

                        }


                    }
                }
                catch (Exception ex)
                { }
                finally
                {
                    command.Connection.Close();
                }
            }
                          
            return extractedUsers;
        }

        

        #endregion

        //*********************************************************************
        // PRIVATE
        //*********************************************************************
        #region Private

        private String GetStringFromObj(object value)
        {
            if (value != DBNull.Value)
                return (string)value;
            return null;
        }


        private DateTime GetDateTimeFromObj(object value)
        {
            if (value != DBNull.Value)
                return (DateTime)value;
            return DateTime.MinValue;
        }

        private int GetIntFromString(string str)
        {
            int result = 0;
            if (!int.TryParse(str, out result))
                return 0;
            return result;
        }

        private float GetFloatFromString(string str)
        {
            float result = 0;

            if (!float.TryParse(str, out result))
                return 0;

            return result;
        }

        #endregion
    }
}
