﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMSSql.Data
{
    public class ExtractedUsers
    {

        public Guid attempt_id { get; set; }
        public Guid session_id { get; set; }
        public string step { get; set; }
        public string sub_step { get; set; }
        public string group { get; set; }
        public string tag { get; set; }
        public string meta { get; set; }
        public DateTime created { get; set; }
        public DateTime modified { get; set; }
        public int size { get; set; }
        public int complete { get; set; }
        public DateTime completed { get; set; }
        public DateTime step_start_date { get; set; }
        public double score { get; set; }
        public DateTime step_duration { get; set; }
        public Guid user_id { get; set; }
        public string manifest_id { get; set; }
        public Guid org_id { get; set; }
        public Guid institution_id { get; set; }

    }
}
