﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace FunctionApp2
{
    public class MessageToStream
    {
               
        public int id { get; set; }
        public string name { get; set; }
        public string headline { get; set; }
        public string text { get; set; }
        public int product_id { get; set; }
        public bool show_evaluations { get; set; }
        public int total_questions { get; set; }
        public bool random_sections { get; set; }
        public bool random_questions { get; set; }
        public string css { get; set; }
        public string dbName { get; set; }
        public string token { get; set; }
        public Guid user_id { get; set; }

        
       
        public List<Question> QuestionsForUser { get; set; }
        
    }

    public class RootObject
    {
        public string id { get; set; }
        public string name { get; set; }
        public string headline { get; set; }
        public string text { get; set; }
        public string product_id { get; set; }
        public string show_evaluations { get; set; }
        public string total_questions { get; set; }
        public string random_sections { get; set; }
        public string random_questions { get; set; }
        public string css { get; set; }
        public string dbName { get; set; }
        public string token { get; set; }
        public string questionId { get; set; }
        public string selectedOptionId { get; set; }  
        public string isCorrectOption { get; set; }
        public long timestamp { get; set; }
        public Guid user_id { get; set; }
        public DateTime completed { get; set; }
        public DateTime step_start_date { get; set; }
        public double score { get; set; }
        public DateTime step_duration { get; set; }
        public string manifest_id { get; set; }
        public Guid org_id { get; set; }
        public Guid institution_id { get; set; }
        public DateTime created { get; set; }
    }
}
