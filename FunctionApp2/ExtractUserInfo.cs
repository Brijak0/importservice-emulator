using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using BlobManager;
using AzureMSSql;
using AzureMSSql.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;




namespace FunctionApp2
{
    public static class ExtractUserInfo
    {
        [FunctionName("ExtractUserInfo")]
        public static void Run([TimerTrigger("0 */5 * * * *")]TimerInfo myTimer, TraceWriter log)
        {
            log.Info($"C# Timer trigger function executed at: {DateTime.Now}");
            string connectionStr = System.Environment.GetEnvironmentVariable("AzureSqlDbConnection", EnvironmentVariableTarget.Process);
            string jobTimeStamp = System.Environment.GetEnvironmentVariable("AzureWebJobTimeStampStorage", EnvironmentVariableTarget.Process);
            string csBlobConnection = System.Environment.GetEnvironmentVariable("AzureWebJobCSStorageStorage", EnvironmentVariableTarget.Process);
            string kafkaBroker = System.Environment.GetEnvironmentVariable("KafkaBrokerList", EnvironmentVariableTarget.Process);
            string topic = System.Environment.GetEnvironmentVariable("TopicName", EnvironmentVariableTarget.Process);
            bool isMessageAvailable = true;
            do
            {

                //1) Get last run date from storage
                BlobMgr blobMgr = new BlobMgr(jobTimeStamp, csBlobConnection);
                DateTime lastDateTime = blobMgr.GetLastDateTime();
                DateTime nextDay = lastDateTime + new TimeSpan(1, 0, 0, 0);
                DateTime currentDay = DateTime.UtcNow;
                TimeSpan ts = currentDay - nextDay;
                if (ts.Days < 1)
                    return;

                ////2) Select data from one day 
                AzureMSSqlMgr azureMSSqlMgr = new AzureMSSqlMgr(connectionStr);
                List<ExtractedUsers> users = azureMSSqlMgr.GetExtractedUsers(lastDateTime, nextDay);

                //////3) Get blob files
                List<string> jsonMessages = new List<string>();
                foreach (var user in users)
                {
                    string blobName = GetBlobName(user.created, user.session_id);
                    string blobFolder = GetBlobFolder(user.created);
                    string blobSubFolder = GetBlobSubFolder(user.created);
                    MemoryStream stream = new MemoryStream();
                    try
                    {
                        stream = blobMgr.GetCSBlobForAssignment(blobFolder, blobSubFolder, blobName);
                    }
                    catch (Exception ex)
                    {
                        log.Info(string.Format("Exception in getting CS blob: {0}", ex));
                    }
                    //4) Make the json file for SPARK  
                    if (stream.Length > 0)
                    {
                        List<RootObject> datas = GetMessage(stream, user.user_id);
                        foreach (var data in datas)
                        {
                            if (!string.IsNullOrEmpty(data.id))
                            {
                                data.completed = user.completed;
                                data.step_start_date = user.step_start_date;
                                data.score = user.score;
                                data.step_duration = user.step_duration;
                                data.manifest_id = user.manifest_id;
                                data.org_id = user.org_id;
                                data.institution_id = user.institution_id;
                                data.created = user.created;
                                jsonMessages.Add(JsonConvert.SerializeObject(data));
                            }
                        }

                    }
                }
                //5) Send the data using Kafka to SPARK if message count  > 0
                if (jsonMessages.Count > 0)
                {
                    isMessageAvailable = true;
                    var config = new Dictionary<string, object> { { "bootstrap.servers", kafkaBroker } };
                    using (var producer = new Producer<Null, string>(config, null, new StringSerializer(Encoding.UTF8)))
                    {
                        //    Console.WriteLine($"{producer.Name} producing on {topic}. q to exit.");

                        foreach (string str in jsonMessages)
                        {
                            log.Info(string.Format("Sending: {0}", str));
                            var deliveryReport = producer.ProduceAsync(topic, null, str);
                            var result = deliveryReport.Result;
                        }
                    }
                    //    // Tasks are not waited on synchronously (ContinueWith is not synchronous),
                    //    // so it's possible they may still in progress here.
                }
                else
                {
                    isMessageAvailable = false;
                    log.Info(string.Format("No messages found pull data for next day: {0}", nextDay.ToShortDateString()));
                }

                //6) update last run data in blob storage
                if (!blobMgr.SetLastDateTime(nextDay))
                {
                    log.Info("Error writting last dt");
                }
            } while (!isMessageAvailable);

            

            
        }

        private static List<RootObject> GetMessage(MemoryStream stream, Guid user_id)
        {
            MessageToStream message = null;
            try
            {
                
                var t = typeof(RootObject);
               
                byte[] mem = DecompressGZip(stream.ToArray());
                StreamReader reader = new StreamReader(new MemoryStream(mem));
                string text = reader.ReadToEnd();
                var results = JsonConvert.DeserializeObject<List<RootObject>>(text);
                if (results != null)
                {
                    foreach(var result in results)
                    {
                        result.user_id = user_id;
                        if (string.IsNullOrEmpty(result.name))
                        {
                            //Get the one from the list which is the root
                            //fill in the blanks
                            var res = results.Find(e => (!string.IsNullOrEmpty(e.name)));
                            result.css = res.css;
                            result.dbName = res.dbName;
                            result.headline = res.headline;
                            result.id = res.id;
                            result.name = res.name;
                            result.product_id = res.product_id;
                            result.token = res.token;
                            result.questionId = res.questionId ?? "-1";
                            result.total_questions = res.total_questions;
                            result.random_questions = res.random_questions;
                            result.random_sections = res.random_sections;
                            result.show_evaluations = res.show_evaluations;
                            result.selectedOptionId = res.selectedOptionId ?? "-1";
                            result.text = res.text;
                         }
                    }
                }
                //Console.WriteLine("Decompressed: {0}", fileToDecompress.Name);

                return results;  
             }
            
            catch(Exception ex)
            {
                throw ex;
            }
            
            return null;
        }

        public static byte[] DecompressGZip(byte[] bytesToDecompress)
        {
            using (GZipStream stream = new GZipStream(new MemoryStream(bytesToDecompress), CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    int count;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memoryStream.Write(buffer, 0, count);
                        }
                    } while (count > 0);
                    return memoryStream.ToArray();
                }
            }
        }

        private static string GetBlobSubFolder(DateTime created) =>                 
            GetCorrectFormat(created.Hour);
               

        private static string GetCorrectFormat(int value)
        {
            if (value < 10)
                return "0" + value;

            return string.Format("{0}", value);
        }
        private static string GetBlobFolder(DateTime created)
        {
            string year = created.Year.ToString();
            string mm = GetCorrectFormat(created.Month);
            string day = GetCorrectFormat(created.Day);
            return year + "-" + mm + "-" + day;
        }

        private static string GetBlobName(DateTime created, Guid session_id)
        {
            string year = created.Year.ToString();
            string mm = GetCorrectFormat(created.Month);
            string day = GetCorrectFormat(created.Day);
            string hour = GetCorrectFormat(created.Hour);
            string min = GetCorrectFormat(created.Minute);
            string sec = GetCorrectFormat(created.Second);
            string ms = GetCorrectFormat(created.Millisecond);
            string session = session_id.ToString().ToUpper();
            return year + mm + day + "_" + hour + min + sec + ms + "_" + session + ".blob";
        }
    }
}
