﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionApp2
{
    public class Question
    {
            public int QuestionId { get; set; }
            public int SelectedOptionId { get; set; }
            public bool IsCorrectOption { get; set; }
            public DateTime Timestamp { get; set; }
        
    }
}
